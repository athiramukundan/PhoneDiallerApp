
public class OnlineState implements State {

	Phone phone;
	
	public  OnlineState(Phone phone) {
		// TODO Auto-generated constructor stub
		this.phone = phone;
	}
	private CallInfo currentCallInfo, lastCallInfo;
	
	@Override
	public void status() {
		// TODO Auto-generated method stub
		System.out.println("Switched On");
	}

	@Override
	public void dial(String number) {
		// TODO Auto-generated method stub
		if(!checkIfValidNumber(number)){
			System.out.println("This is not a valid number:" + number);
			return;
		}
		String callType = checkIfInternational(number) ? "international" : "national";
		System.out.println("Making an " + callType + " call to: " +  number);
		currentCallInfo = new CallInfo(number);
		currentCallInfo.dial();
		
	}

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		if(currentCallInfo == null || !currentCallInfo.isCallInProgress()){
			System.out.println("No call in progress");
			return;
		}
		System.out.println("Call disconnected");
		lastCallInfo = currentCallInfo;
	}

	@Override
	public void lastCallInfo() {
		if(lastCallInfo != null){
			System.out.println("Called "+ lastCallInfo.getPhoneNumber() +". Duration "
					+ lastCallInfo.getTimeSpentInSeconds() +" seconds.");
			return;
		}
		System.out.println("No call history");
	}

	@Override
	public void switchOn() {
		// TODO Auto-generated method stub
		System.out.println("Already Switched On");
	}

	@Override
	public void switchOff() {
		// TODO Auto-generated method stub
		System.out.println("Switching Off...");
		phone.setState(new OfflineState(phone));
	}

	@Override
	public void aeroplaneModeOn() {
		// TODO Auto-generated method stub
		System.out.println("Going into airplane mode...");
		phone.setState(new AeroplaneState(phone));
	}

	@Override
	public void aeroplaneModeOff() {
		// TODO Auto-generated method stub
		System.out.println("Not in airplane mode.");
	}
	
	private Boolean checkIfValidNumber(String number){
		if (number.matches("\\d{10}")) return true;
		else if(number.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
		else if(number.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
		else if(number.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
		else return false;
	
	}
	
	private Boolean checkIfInternational(String number){
		return !(number.startsWith("+91") || number.startsWith("91") || number.length() == 10);
	}

}
