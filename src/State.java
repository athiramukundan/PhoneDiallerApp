
public interface State {
	void dial(String number);
	void disconnect();
	void lastCallInfo();
	void status();
	void switchOn();
	void switchOff();
	void aeroplaneModeOn();
	void aeroplaneModeOff();

}
