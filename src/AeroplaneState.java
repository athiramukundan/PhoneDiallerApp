
public class AeroplaneState implements State {
	Phone phone;
	
	public  AeroplaneState(Phone phone) {
		// TODO Auto-generated constructor stub
		this.phone = phone;
	}
	@Override
	public void status() {
		// TODO Auto-generated method stub
		System.out.println("In Aeroplane mode");
	}

	@Override
	public void dial(String number) {
		// TODO Auto-generated method stub
		System.out.println("Cannot make calls when in airplane mode.");
		
	}

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		System.out.println("No call in progress");
	}

	@Override
	public void lastCallInfo() {
		// TODO Auto-generated method stub
		System.out.println("In airplane mode");
	}

	@Override
	public void switchOn() {
		// TODO Auto-generated method stub
		System.out.println("Already Switched On");
	}

	@Override
	public void switchOff() {
		// TODO Auto-generated method stub
		System.out.println("Switching Off�");
		phone.setState(new OfflineState(phone));
	}

	@Override
	public void aeroplaneModeOn() {
		// TODO Auto-generated method stub
		System.out.println("Already in airplane mode");
	}

	@Override
	public void aeroplaneModeOff() {
		// TODO Auto-generated method stub
		System.out.println("AeroplaneMode Off");
		phone.setState(new OnlineState(phone));
	}

}
