
public class OfflineState implements State {
	
	Phone phone;
	
	public  OfflineState(Phone phone) {
		// TODO Auto-generated constructor stub
		this.phone = phone;
	}
	@Override
	public void status() {
		// TODO Auto-generated method stub
		System.out.println("Switched Off");
	}

	@Override
	public void dial(String number) {
		// TODO Auto-generated method stub
		System.out.println("Cannot make calls when switched off.");
		
	}

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		System.out.println("No call in progress");
	}

	@Override
	public void lastCallInfo() {
		// TODO Auto-generated method stub
	}

	@Override
	public void switchOn() {
		// TODO Auto-generated method stub
		System.out.println("Booting up.");
		phone.setState(new OnlineState(phone));
	}

	@Override
	public void switchOff() {
		// TODO Auto-generated method stub
		System.out.println("Already Switched Off");
	}

	@Override
	public void aeroplaneModeOn() {
		// TODO Auto-generated method stub
		System.out.println("Cannot go into airplane mode when switched off");
	}

	@Override
	public void aeroplaneModeOff() {
		// TODO Auto-generated method stub
		System.out.println("Not in airplane mode. Current status is switched off");
	}
}
