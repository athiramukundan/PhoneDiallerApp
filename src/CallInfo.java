
public class CallInfo {
	private String phoneNumber;
	private Boolean callInProgress;
	private long timeSpentInMillis;
	private long startTime;
	
	public CallInfo(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public void dial(){
		this.callInProgress = true;
		startTime = System.currentTimeMillis();
	}
	
	public void disconnect(){
		timeSpentInMillis = System.currentTimeMillis() - startTime;
		this.callInProgress = false;
	}
	
	public String getPhoneNumber(){
		return phoneNumber;
	}
	
	public long getTimeSpentInSeconds(){
		return timeSpentInMillis/1000;
	}
	
	public Boolean isCallInProgress()
	{
		return callInProgress;
	}

}
