
public class Phone {
	
	State currentState;
	public Phone(){
		currentState = new OnlineState(this);
	}

	public void status() {
		currentState.status();
	}


	public void dial(String number) {
		// TODO Auto-generated method stub
		currentState.dial(number);
	}


	public void disconnect() {
		currentState.disconnect();
	}


	public void lastCallInfo() {
		currentState.lastCallInfo();
	}


	public void switchOn() {
		// TODO Auto-generated method stub
		currentState.lastCallInfo();
	}


	public void switchOff() {
		currentState.switchOff();
	}


	public void aeroplaneModeOn() {
		// TODO Auto-generated method stub
		currentState.aeroplaneModeOn();
	}


	public void aeroplaneModeOff() {
		// TODO Auto-generated method stub
		currentState.aeroplaneModeOff();
	}
	public void setState(State state){
		currentState = state;
	}
}
